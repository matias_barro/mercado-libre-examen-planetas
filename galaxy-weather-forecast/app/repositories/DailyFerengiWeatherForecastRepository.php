<?php
namespace Repositories;

require_once(dirname(__FILE__) . "/../../vendor/autoload.php");

class DailyFerengiWeatherForecastRepository 
{
	const MAX_INSERTS = 100;
	private static $mongoDbCollection; 
	
	private static function getInstance(){
		if(!self::$mongoDbCollection){
			self::$mongoDbCollection =  (new \MongoDB\Client)->ferengisolarsystem->dailyweatherforecast;
		}

		return self::$mongoDbCollection;
	}

	public function insertOne($dailyWeatherForecast){
		$insertOneResult = self::getInstance()->insertOne(self::formatModel($dailyWeatherForecast));

		if($insertOneResult->getInsertedCount() < 1){
			throw new \Exception("Fails insert", 1);
		}

		return $insertOneResult;
	}

	public function insertMany($dailyWeatherForecast){
		if(count($dailyWeatherForecast) == 0){
			throw new \Exception("No Daily Weather Forecasts to insert", 1);
		}

		if(count($dailyWeatherForecast) > self::MAX_INSERTS){
			throw new \Exception("Max " . self::MAX_INSERTS . " document can be inserted in a single request", 1);
		}

		$toInsert = array_map(function($dailyForecast){
			return self::formatModel($dailyForecast);
		}, $dailyWeatherForecast);

		$insertManyResult = self::getInstance()->insertMany($toInsert);

		if($insertManyResult->getInsertedCount() != count($dailyWeatherForecast)){
			throw new \Exception("Fails insert", 1);
		}

		return $insertManyResult;
	}

	public function drop(){
		self::getInstance()->drop();
	}

	private function formatModel($dailyweatherforecast){
		return array(
					"day" => $dailyweatherforecast->getDay(),
					"weather" => $dailyweatherforecast->getWeatherForecast()->getType(),
					"magnitude" => $dailyweatherforecast->getWeatherForecast()->getMagnitude()
				);
	}
}