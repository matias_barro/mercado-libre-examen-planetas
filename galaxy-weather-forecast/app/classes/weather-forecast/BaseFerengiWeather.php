<?php
namespace Classes\WeatherForecast;

require_once(dirname(__FILE__) . '/../../models/solar-system/FerengiSolarSystem.php');

use Models\SolarSystem\FerengiSolarSystem;

/*
 * Class to encapsulate logic to determine WeatherType
 *
 * Use of the Chain of Responsability Pattern. If current condition is not present, check * next condition in chain
*/
abstract class BaseFerengiWeather{

	private $nextWeather;

	public function getWeatherForecast(FerengiSolarSystem $ferengiSolarSystem){
		if($this->hasWeather($ferengiSolarSystem)){
			return $this->makeWeatherForecast();
		}else if($this->nextWeather){
			return $this->nextWeather->getWeatherForecast($ferengiSolarSystem);
		}else{
			throw new Exception("Weather not present, no weather to proceed", 1);
		}
	}

	public function setNext(BaseFerengiWeather $nextWeather){
		$this->nextWeather = $nextWeather;
	}

	abstract protected function hasWeather(FerengiSolarSystem $ferengiSolarSystem);
	abstract protected function makeWeatherForecast();
}