<?php
namespace Classes\WeatherForecast;

require_once(dirname(__FILE__) . '/BaseFerengiWeather.php');
require_once(dirname(__FILE__) . '/../../models/solar-system/FerengiSolarSystem.php');
require_once(dirname(__FILE__) . '/../../models/weather-forecast/WeatherForecast.php');
require_once(dirname(__FILE__) . '/../../constants/WeatherTypes.php');

use Models\SolarSystem\FerengiSolarSystem;
use Models\WeatherForecast\WeatherForecast;
use Constants\WeatherTypes;

class FerengiNormalWeather extends BaseFerengiWeather
{
	function __construct(){}

	//no condition to check
	protected function hasWeather(FerengiSolarSystem $ferengiSolarSystem){
		return true;
	}

	protected function makeWeatherForecast(){
		return new WeatherForecast(WeatherTypes::NORMAL);
	}
}