<?php
namespace Classes\WeatherForecast;

require_once(dirname(__FILE__) . '/BaseFerengiWeather.php');
require_once(dirname(__FILE__) . '/../../models/solar-system/FerengiSolarSystem.php');
require_once(dirname(__FILE__) . '/../../models/weather-forecast/WeatherForecast.php');
require_once(dirname(__FILE__) . '/../../constants/WeatherTypes.php');
require_once(dirname(__FILE__) . '/../math/Point.php');
require_once(dirname(__FILE__) . '/../math/Triangle.php');

use Models\SolarSystem\FerengiSolarSystem;
use Models\WeatherForecast\WeatherForecast;
use Constants\WeatherTypes;
use Classes\Math\Point;
use Classes\Math\Triangle;

class FerengiRainWeather extends BaseFerengiWeather
{
	private $triangle;

	function __construct(){
		$this->triangle = null;
	}

	//check if sun is inside planets triangle
	protected function hasWeather(FerengiSolarSystem $ferengiSolarSystem){
		$ferengiPlanet = $ferengiSolarSystem->getFerengiPlanet();
		$vulcanoPlanet = $ferengiSolarSystem->getVulcanoPlanet();
		$betazoidePlanet = $ferengiSolarSystem->getBetazoidePlanet();

		$ferengiLocation = new Point($ferengiPlanet->getOrbit()->getXCoordinate(),$ferengiPlanet->getOrbit()->getYCoordinate());

		$vulcanoLocation = new Point($vulcanoPlanet->getOrbit()->getXCoordinate(),$vulcanoPlanet->getOrbit()->getYCoordinate());

		$betazoideLocation = new Point($betazoidePlanet->getOrbit()->getXCoordinate(),$betazoidePlanet->getOrbit()->getYCoordinate());

		$sun = new Point(0,0);

		$this->triangle = new Triangle($ferengiLocation,$vulcanoLocation,$betazoideLocation);

		return $this->triangle->isInside($sun);
	}

	protected function makeWeatherForecast(){
		return ($this->triangle) ? new WeatherForecast(WeatherTypes::RAIN, $this->triangle->perimeter()) : new WeatherForecast(WeatherTypes::RAIN);
	}
}