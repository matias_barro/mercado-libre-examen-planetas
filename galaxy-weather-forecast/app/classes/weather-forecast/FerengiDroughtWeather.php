<?php
namespace Classes\WeatherForecast;

require_once(dirname(__FILE__) . '/BaseFerengiWeather.php');
require_once(dirname(__FILE__) . '/../../models/solar-system/FerengiSolarSystem.php');
require_once(dirname(__FILE__) . '/../../models/weather-forecast/WeatherForecast.php');
require_once(dirname(__FILE__) . '/../../constants/WeatherTypes.php');
require_once(dirname(__FILE__) . '/../math/Point.php');
require_once(dirname(__FILE__) . '/../math/PointAlignment.php');

use Models\SolarSystem\FerengiSolarSystem;
use Models\WeatherForecast\WeatherForecast;
use Constants\WeatherTypes;
use Classes\Math\Point;
use Classes\Math\PointAlignment;

class FerengiDroughtWeather extends BaseFerengiWeather
{
	function __construct(){}

	//check if planets and sun are aligned
	protected function hasWeather(FerengiSolarSystem $ferengiSolarSystem){
		$ferengiPlanet = $ferengiSolarSystem->getFerengiPlanet();
		$vulcanoPlanet = $ferengiSolarSystem->getVulcanoPlanet();
		$betazoidePlanet = $ferengiSolarSystem->getBetazoidePlanet();

		$ferengiLocation = new Point($ferengiPlanet->getOrbit()->getXCoordinate(),$ferengiPlanet->getOrbit()->getYCoordinate());

		$vulcanoLocation = new Point($vulcanoPlanet->getOrbit()->getXCoordinate(),$vulcanoPlanet->getOrbit()->getYCoordinate());

		$betazoideLocation = new Point($betazoidePlanet->getOrbit()->getXCoordinate(),$betazoidePlanet->getOrbit()->getYCoordinate());

		$sun = new Point(0,0);

		return (PointAlignment::areAligned($sun,$ferengiLocation,$betazoideLocation) && PointAlignment::areAligned($sun,$ferengiLocation,$vulcanoLocation));
	}

	protected function makeWeatherForecast(){
		return new WeatherForecast(WeatherTypes::DROUGHT);
	}
}