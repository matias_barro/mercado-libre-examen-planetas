<?php
Namespace Classes\WeatherForecast;

require_once(dirname(__FILE__) . '/../../models/solar-system/FerengiSolarSystem.php');
require_once(dirname(__FILE__) . '/FerengiDroughtWeather.php');
require_once(dirname(__FILE__) . '/FerengiNormalWeather.php');
require_once(dirname(__FILE__) . '/FerengiOptimalWeather.php');
require_once(dirname(__FILE__) . '/FerengiRainWeather.php');

use Models\SolarSystem\FerengiSolarSystem;


class FerengiWeatherForecaster{

	private $weatherChain;

	public function __construct(){
		$this->initializeWeatherChain();
	}

	/*
	 * Check weather condition depending on planet positions
	 * 
	 *  RAIN: Planets make a triangle. Sun is inside
	 *  DROUGHT: Planets and sun aligned
	 *  OPTIMAL_CONDITIONS: Planets aligned 
	 */
	public function weatherForecast(FerengiSolarSystem $ferengiSolarSystem){
		return $this->weatherConditionChain->getWeatherForecast($ferengiSolarSystem);
	}
	
	private function initializeWeatherChain(){
		//normal condition
		$normalWeatherCondition = new FerengiNormalWeather();
		
		//rain condition => normal condition
		$rainWeatherCondition = new FerengiRainWeather();
		$rainWeatherCondition->setNext($normalWeatherCondition);

		//optimal condition => rain condition => normal condition
		$optimalWeatherCondition = new FerengiOptimalWeather();
		$optimalWeatherCondition->setNext($rainWeatherCondition);

		//drought condition => optimal condition => rain condition => normal condition
		$droughtWeatherCondition = new FerengiDroughtWeather();
		$droughtWeatherCondition->setNext($optimalWeatherCondition);

		$this->weatherConditionChain = $droughtWeatherCondition;
	}

}

