<?php
namespace Classes\Math;

require_once(dirname(__FILE__) . '/Point.php');
require_once(dirname(__FILE__) . '/PointAlignment.php');


class Triangle{

	private $vertexA;
	private $vertexB;
	private $vertexC;

	function __construct(Point $vertexA, Point $vertexB, Point $vertexC){
		$this->vertexA = $vertexA;
		$this->vertexB = $vertexB;
		$this->vertexC = $vertexC;
	}

	/*
	 * Calculates triangle orientation using this formula:
	 * 
	 *  (vertexA.x - vertexC.x) * (vertexB.y - vertexC.y) 
	 *	- 
	 *	(vertexA.y - vertexC.y) * (vertexB.x - vertexC.x)
	 *
	 *	returns 1 for positive orientation, -1 for negative orientation	 	
	 */
	public function orientation(){
		$partialResultA = ($this->vertexA->getXCoordinate() - $this->vertexC->getXCoordinate()) * ($this->vertexB->getYCoordinate() - $this->vertexC->getYCoordinate());
	
		$partialResultB = ($this->vertexA->getYCoordinate() - $this->vertexC->getYCoordinate()) * ($this->vertexB->getXCoordinate() - $this->vertexC->getXCoordinate());

		return ($partialResultA - $partialResultB >= 0) ? 1 : -1; 
	}

	/*
     *  Using triangle orientation to determine if a point is inside it.
     * 
	 *	ABC triangle and P point // P is inside ABC triangle if ABP, BCP and CAP
	 *  triangles' orientation is the same as ABC
	 *
	 */
	public function isInside(Point $point){
		$abpTriangle = new Triangle($this->vertexA,$this->vertexB,$point);
		$bcpTriangle = new Triangle($this->vertexB,$this->vertexC,$point);
		$capTriangle = new Triangle($this->vertexC,$this->vertexA,$point);

		return ($this->orientation() == $abpTriangle->orientation() &&
				 $abpTriangle->orientation() == $bcpTriangle->orientation() &&
				 $bcpTriangle->orientation() == $capTriangle->orientation()
				);
	}

	public function perimeter(){
		$abSideLength = $this->sideLength($this->vertexA,$this->vertexB);
		$bcSideLength = $this->sideLength($this->vertexB,$this->vertexC);
		$caSideLength = $this->sideLength($this->vertexC,$this->vertexA);

		$perimeter = $abSideLength + $bcSideLength + $caSideLength;
		return round($perimeter,2);
	}

	private function sideLength(Point $pointA, Point $pointB){
		$sideLength = sqrt(pow($pointA->getXCoordinate() - $pointB->getXCoordinate(),2) + pow($pointA->getYCoordinate() - $pointB->getYCoordinate(),2));

		return $sideLength;
	}
}