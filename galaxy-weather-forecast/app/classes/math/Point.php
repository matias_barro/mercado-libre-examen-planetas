<?php
namespace Classes\Math;

class Point{

	private $x;
	private $y;

	function __construct(float $x, float $y){
		$this->x = $x;
		$this->y = $y;
	}

	public function getXCoordinate(){
		return $this->x;
	}

	public function getYCoordinate(){
		return $this->y;
	}
}