<?php
namespace Classes\Math;

require_once(dirname(__FILE__) . '/Point.php');

class PointAlignment{

	/*
	 *  Check if points are aligned using this formula:
	 * 
	 *   p2Y - p1Y     p3Y - p2Y
	 *   ---------  =  ---------
	 *   p2X - p1X 	   p3X - p2X
	 */
	public static function areAligned(Point $p1, Point $p2, Point $p3){
		$p2p1Numerator  = $p2->getYCoordinate() - $p1->getYCoordinate();
		$p2p1Denominator  = $p2->getXCoordinate() - $p1->getXCoordinate();
		
		$p3p2Numerator  = $p3->getYCoordinate() - $p2->getYCoordinate();
		$p3p2Denominator  = $p3->getXCoordinate() - $p2->getXCoordinate();

		if($p2p1Denominator == 0 || $p3p2Denominator == 0){
			//only aligned if both denominators are zero
			return ($p2p1Denominator == 0 && $p3p2Denominator == 0);
		}

		$p2p1Slope = $p2p1Numerator / $p2p1Denominator;
		$p3p2Slope = $p3p2Numerator / $p3p2Denominator;

		
		return (abs($p2p1Slope - $p3p2Slope) < 0.1);
	}
}