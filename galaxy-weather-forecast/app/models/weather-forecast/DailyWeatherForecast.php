<?php
Namespace Models\WeatherForecast;

class DailyWeatherForecast{
	private $weatherForecast;
	private $day;

	function __construct($day, $weatherForecast){
		if($day <= 0){
			throw new \Exception("Day cannot be zero", 1);
		}

		$this->day = $day;
		$this->weatherForecast = $weatherForecast;
	}

	public function getDay(){
		return $this->day;
	}

	public function getWeatherForecast(){
		return $this->weatherForecast;
	}
}