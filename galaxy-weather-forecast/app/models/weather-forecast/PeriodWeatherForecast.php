<?php 
namespace Models\WeatherForecast;

require_once(dirname(__FILE__) . '/DailyWeatherForecast.php');
require_once(dirname(__FILE__) . '/../../constants/WeatherTypes.php');

use Constants\WeatherTypes;

class PeriodWeatherForecast
{
	private $dailyForecasts;
	//index map WEATHER => [days];
	private $dailyForecastsByWeather;

	function __construct(){
		$this->dailyForecasts = array();
		$this->dailyForecastsByWeather = array();
	}

	public function addDailyForecast(DailyWeatherForecast $dailyForecast){
		if(array_key_exists($dailyForecast->getDay(), $this->dailyForecasts)){
			throw new \Exception("Daily forecast already exists", 1);
		}

		$this->dailyForecasts[$dailyForecast->getDay()] = $dailyForecast;
		$this->dailyForecastsByWeather[$dailyForecast->getWeatherForecast()->getType()][] = $dailyForecast->getDay();
	}	

	public function getDailyForecasts(){
		return $this->dailyForecasts;
	}

	public function getCountDailyForecastsByWeather($weatherType){
		//check if it's a valid weather type
		if(!in_array($weatherType, WeatherTypes::getTypeList())){
			throw new \Exception("Invalid weather type", 1);
		}

		if(array_key_exists($weatherType, $this->dailyForecastsByWeather)){
			return count($this->dailyForecastsByWeather[$weatherType]);
		}else{
			return 0;
		}
	}

	/*
	 * returns weather forecast of max rain day.
	 * 
	 * if there are no rainy days throws exception
	 */
	public function getPeriodMaxRainDay(){
		if(!array_key_exists(WeatherTypes::RAIN, $this->dailyForecastsByWeather)){
			throw new \Exception("No rainy days", 1);
		}

		$maxRainDailyWeatherForecast = null;
		foreach ($this->dailyForecastsByWeather[WeatherTypes::RAIN] as $rainDay) {
			
			$rainDayForecast = $this->dailyForecasts[$rainDay];
			if($maxRainDailyWeatherForecast == null || $rainDayForecast->getWeatherForecast()->getMagnitude() > $maxRainDailyWeatherForecast->getWeatherForecast()->getMagnitude()){
				$maxRainDailyWeatherForecast = $rainDayForecast;
			}
		}

		return $maxRainDailyWeatherForecast;
	}


}
