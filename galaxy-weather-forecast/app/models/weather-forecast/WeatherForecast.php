<?php
Namespace Models\WeatherForecast;

require_once(dirname(__FILE__) . '/../../constants/WeatherTypes.php');
use Constants\WeatherTypes;

class WeatherForecast{
 
	private $type;
	private $magnitude;

	public function __construct($type, $magnitude = 0){
		if(!in_array($type, WeatherTypes::getTypeList())){
			throw new \Exception("Invalid weather type", 1);
		}

		$this->type = $type;
		$this->magnitude = $magnitude;
	}

	public function getType(){
		return $this->type;
	}

	public function getMagnitude(){
		return $this->magnitude;
	}

}