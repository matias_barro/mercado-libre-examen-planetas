<?php
Namespace Models\SolarSystem;

require_once(dirname(__FILE__) . '/../../interfaces/TimeElapse.php');
require_once(dirname(__FILE__) . '/Planet.php');

use Interfaces\TimeElapse;
use Models\SolarSystem\Planet;

class SolarSystem implements TimeElapse{

	/*
	 * Map with all SolarSystem planets.
	 * 
	 *  PlanetName --> Planet 
	 */
	private $planets;

	public function __construct(){
		$this->planets = array();
	}

	public function getPlanets(){
		return $this->planets;
	}

	public function addPlanet($planet){
		$this->checkCanAddPlanet($planet);
		$this->planets[$planet->getName()] = $planet;
	}

	public function getPlanetByName($planetName){
		$this->checkPlanetExistence($planetName);
		return $this->planets[$planetName];
	}


	public function removePlanet($planetName){
		$this->checkPlanetExistence($planetName);
		unset($this->planets[$planetName]);	
	}

	/*
	 * Elapse days on each planet
	*/
	public function elapseDays($days){
		foreach ($this->planets as &$solarSystemPlanet) {
			$solarSystemPlanet->elapseDays($days);
		}
	}


	/*
	* Validate if new planet could be added to Solar System
	* 
	*  # Two planets with same name cannot be added
	*  # Planet with same sun distance as other planet cannot be added
	*/
	private function checkCanAddPlanet($planet){
		if(array_key_exists($planet->getName(), $this->planets)){
			throw new \Exception("Planet with name '". $planet->getName() . "' already exists", 1);
		}

		$planetCollision = false;
		foreach ($this->planets as $solarSystemPlanet) {
			if($solarSystemPlanet->getOrbit()->getDistanceFromSun() == $planet->getOrbit()->getDistanceFromSun()){
				$planetCollision = true;
				break;
			}
		}

		if($planetCollision){
			throw new \Exception("Planet cannot be in the same orbit as other planet", 1);
		}
	}

	private function checkPlanetExistence($planetName){
		if(!array_key_exists($planetName, $this->planets)){
			throw new \Exception("Planet not found with provided name", 1);
		}
	}

}