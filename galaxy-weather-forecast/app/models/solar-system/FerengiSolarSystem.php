<?php
Namespace Models\SolarSystem;

require_once(dirname(__FILE__) . '/../../interfaces/TimeElapse.php');
require_once(dirname(__FILE__) . '/Planet.php');
require_once(dirname(__FILE__) . '/Orbit.php');
require_once(dirname(__FILE__) . '/SolarSystem.php');

use Interfaces\TimeElapse;
use Models\SolarSystem\SolarSystem;
use Models\SolarSystem\Orbit;
use Models\SolarSystem\Planet;

/*
 * Model to represent Ferengi, Vulcano and Betazoide solar system; 	
*/
class FerengiSolarSystem implements TimeElapse{
	const FERENGI_PLANET_NAME = "ferengi";
	const VULCANO_PLANET_NAME = "vulcano";
	const BETAZOIDE_PLANET_NAME = "betazoide";

	private $solarSystem;

	public function __construct(){
		$this->solarSystem = new SolarSystem();

		//add farengi
		$this->solarSystem->addPlanet(new Planet(self::FERENGI_PLANET_NAME, new Orbit(500,-1,90)));
		//add vulcano
		$this->solarSystem->addPlanet(new Planet(self::VULCANO_PLANET_NAME, new Orbit(1000,5,90)));
		//add betazoide
		$this->solarSystem->addPlanet(new Planet(self::BETAZOIDE_PLANET_NAME, new Orbit(2000,-3,90)));
	}

	public function getFerengiPlanet(){
		return $this->getPlanet(self::FERENGI_PLANET_NAME);
	}

	public function getVulcanoPlanet(){
		return $this->getPlanet(self::VULCANO_PLANET_NAME);
	}

	public function getBetazoidePlanet(){
		return $this->getPlanet(self::BETAZOIDE_PLANET_NAME);
	}

	private function getPlanet($planetId){
		return $this->solarSystem->getPlanetByName($planetId);
	}

	/*
	 * Elapses time in Solar System
	*/
	public function elapseDays($days){
		$this->solarSystem->elapseDays($days);
	}
}