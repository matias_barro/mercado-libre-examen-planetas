<?php
Namespace Models\SolarSystem;

require_once(dirname(__FILE__) . '/../../interfaces/TimeElapse.php');
use Interfaces\TimeElapse;

class Orbit implements TimeElapse{
	private $distanceFromSun;
	private $angularSpeed;
	private $initialAngle;
	private $angle;

	public function __construct(float $distanceFromSun, float $angularSpeed, float $initialAngle = 0){
		if($distanceFromSun <= 0){
			throw new \Exception("Distance from sun must be greater than zero", 1);
		}

		$this->distanceFromSun = $distanceFromSun;
		$this->angularSpeed = $angularSpeed;
		$this->initialAngle = $initialAngle;
		$this->angle = $initialAngle;
	}

	public function getDistanceFromSun(){
		return $this->distanceFromSun;
	}

	public function getAngle(){
		return $this->angle;
	}

	public function elapseDays($days){
		if($days < 0){
			throw new \Exception("Days must be greater then zero", 1);
		}

		$angularMovement = $this->angularSpeed * $days;
		$this->angle = $this->initialAngle + $angularMovement;
	}

	public function getXCoordinate(){
		return $this->distanceFromSun * cos(deg2rad($this->angle));
	}

	public function getYCoordinate(){
		return $this->distanceFromSun * sin(deg2rad($this->angle));
	}

}