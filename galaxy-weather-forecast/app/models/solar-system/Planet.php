<?php
Namespace Models\SolarSystem;

require_once(dirname(__FILE__) . '/../../interfaces/TimeElapse.php');
require_once(dirname(__FILE__) . '/Orbit.php');

use Interfaces\TimeElapse;
use Models\SolarSystem\Orbit;

class Planet implements TimeElapse{
	
	private $name;
	private $orbit;

	public function __construct($name, Orbit $orbit){
		if(!$name){
			throw new \Exception("Name must be provided", 1);
		}

		if(!$orbit){
			throw new \Exception("Orbit must be provided", 1);
		}

		$this->name = $name;
		$this->orbit = $orbit;
	}

	public function getName(){
		return $this->name;
	}

	public function getOrbit(){
		return $this->orbit;
	}

	public function elapseDays($days){
		//elapse days in orbit
		$this->orbit->elapseDays($days);
	}
}