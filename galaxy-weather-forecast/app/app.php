<?php
require_once(dirname(__FILE__) . '/services/FerengiWeatherForecastService.php');
require_once(dirname(__FILE__) . '/constants/WeatherTypes.php');

use Services\FerengiWeatherForecastService;
use Constants\WeatherTypes;

/*
 *
 *  Farengi solar system weather forecast
 *  
 *  Displays summary of a period's weather forecast
 *
 *  Parameters:
 *   -y years 
 *   -m months
 *   -d days
 *
 *   All parameters' value must be greater than zero and either one of then should be 		*   provided
 */	

$parameters = getopt("y::m::d::");
checkParameters($parameters);
$periodDays = getPeriodDays($parameters);

$forecastService = new FerengiWeatherForecastService();
$periodWeatherForecast = $forecastService->getWeatherForecast($periodDays);

echo "*****************************************" . "\n";
echo "* Farengi Solar System Weather Forecast *" . "\n";
echo "*****************************************" . "\n\n";

echo "Total period days: " . $periodDays . "\n\n";

echo "Rain: " . $periodWeatherForecast->getCountDailyForecastsByWeather(WeatherTypes::RAIN) . " days\n";

echo "Drought: " . $periodWeatherForecast->getCountDailyForecastsByWeather(WeatherTypes::DROUGHT) . " days\n";

echo "Optimal: " . $periodWeatherForecast->getCountDailyForecastsByWeather(WeatherTypes::OPTIMAL) . " days\n\n";

echo "Max rain day: " . $periodWeatherForecast->getPeriodMaxRainDay()->getDay() . "\n\n";


function checkParameters($parameters){
	if(!array_key_exists("y", $parameters) && !array_key_exists("m", $parameters) && !array_key_exists("d", $parameters)){
		echo "Either year (-y), month (-m) or day (-d) must be provided\n\n";
		exit;
	}

	if(array_key_exists("y", $parameters) && $parameters["y"] == 0){
		echo "Years value must be greater than zero\n\n";
		exit;
	}

	if(array_key_exists("m", $parameters) && $parameters["m"] == 0){
		echo "Months value must be greater than zero\n\n";
		exit;	
	}	

	if(array_key_exists("d", $parameters) && $parameters["d"] == 0){
		echo "Days value must be greater than zero\n\n";
		exit;	
	}	
}

function getPeriodDays($parameters){
	$periodDays = 0;
	foreach ($parameters as $param => $value) {
		switch ($param) {
			case 'y':
				$periodDays += 365 * $value;
				break;

			case 'm':
				$periodDays += 30 * $value;
				break;

			case 'd':
				$periodDays += $value;
				break;
		}
	}

	return $periodDays;
}