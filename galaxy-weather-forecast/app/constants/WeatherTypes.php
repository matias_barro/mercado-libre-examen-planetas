<?php
Namespace Constants;

class WeatherTypes{
	const RAIN = "rain";
	const DROUGHT = "drought";
	const OPTIMAL = "optimal";
	const NORMAL = "normal";

	public static function getTypeList(){
		return array(self::RAIN,self::DROUGHT,self::OPTIMAL, self::NORMAL);
	}
}