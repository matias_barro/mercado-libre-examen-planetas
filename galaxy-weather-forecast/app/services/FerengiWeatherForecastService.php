<?php
Namespace Services;

require_once(dirname(__FILE__) . '/../models/weather-forecast/PeriodWeatherForecast.php');
require_once(dirname(__FILE__) . '/../models/weather-forecast/DailyWeatherForecast.php');
require_once(dirname(__FILE__) . '/../models/solar-system/FerengiSolarSystem.php');
require_once(dirname(__FILE__) . '/../classes/weather-forecast/FerengiWeatherForecaster.php');

use Models\WeatherForecast\PeriodWeatherForecast;
use Models\WeatherForecast\DailyWeatherForecast;
use Models\SolarSystem\FerengiSolarSystem;
use Classes\WeatherForecast\FerengiWeatherForecaster;

class FerengiWeatherForecastService{

	private $weatherForecaster;

	function __construct(){
		$this->weatherForecaster = new FerengiWeatherForecaster();
	}

	public function getWeatherForecast($days){
		if($days <= 0){
			throw new Exception("Days must be greater than 0", 1);
		}

		$ferengiSolarSystem = new FerengiSolarSystem();
		$periodWeatherForecast = new PeriodWeatherForecast();


		$dayIndex = 1;
		while($dayIndex <= $days){
			$ferengiSolarSystem->elapseDays($dayIndex);
			$weatherForecast = $this->weatherForecaster->weatherForecast($ferengiSolarSystem);
			$periodWeatherForecast->addDailyForecast(new DailyWeatherForecast($dayIndex, $weatherForecast));
			$dayIndex++;
		}

		return $periodWeatherForecast;
	}
}