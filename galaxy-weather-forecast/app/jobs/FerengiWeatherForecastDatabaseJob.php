<?php
require_once(dirname(__FILE__) . "/../repositories/DailyFerengiWeatherForecastRepository.php");
require_once(dirname(__FILE__) . '/../services/FerengiWeatherForecastService.php');

use Repositories\DailyFerengiWeatherForecastRepository;
use Services\FerengiWeatherForecastService;

const MAX_FORECASTS_PER_REQUEST = 100;

echo "***********************************************" . "\n";
echo "* Farengi Solar System Weather Forecast DB JOB*" . "\n";
echo "***********************************************" . "\n\n";

//ten years
$periodDays = 365 * 10;

echo "Total days: " . $periodDays. "\n";

$forecastService = new FerengiWeatherForecastService();
$periodWeatherForecast = $forecastService->getWeatherForecast($periodDays);
$dailyWeatherForecasts = $periodWeatherForecast->getDailyForecasts();

$totalInsertOperations = ceil(count($dailyWeatherForecasts) / MAX_FORECASTS_PER_REQUEST);

echo "Total insert operations: " . $totalInsertOperations . "\n\n";

//drop database and collection
DailyFerengiWeatherForecastRepository::drop();

$offset = 0;
for($op = 1; $op <= $totalInsertOperations; $op++){
  $toInsert = array_slice($dailyWeatherForecasts, $offset,MAX_FORECASTS_PER_REQUEST);
  $offset += MAX_FORECASTS_PER_REQUEST;
  DailyFerengiWeatherForecastRepository::insertMany($toInsert);
  echo "Insert operation completed (" . $op . "/" . $totalInsertOperations . ")\n";
}

echo "\n" . "All forecasts inserted!" . "\n\n";




