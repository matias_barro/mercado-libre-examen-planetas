<?php 
Namespace Interfaces;

interface TimeElapse{
	
	public function elapseDays($days);
}