<?php
require_once(dirname(__FILE__) . '/../../app/classes/math/Triangle.php');
require_once(dirname(__FILE__) . '/../../app/classes/math/Point.php');

use PHPUnit\Framework\TestCase;
use Classes\Math\Point;
use Classes\Math\Triangle;

class TriangleTest extends TestCase{

	public function testPositiveOrientation(){
		$vertexA = new Point(2,2);
		$vertexB = new Point(4,3);
		$vertexC = new Point(3,5);
		$triangle = new Triangle($vertexA,$vertexB,$vertexC);
		
		$this->assertEquals($triangle->orientation(),1);
	}

	public function testNegativeOrientation(){
		$vertexA = new Point(-5,2);
		$vertexC = new Point(0,-4);
		$vertexB = new Point(4,3);
		$triangle = new Triangle($vertexA,$vertexB,$vertexC);
		
		$this->assertEquals($triangle->orientation(),-1);
	}

	public function testContainsPoint(){
		$vertexA = new Point(0,2);
		$vertexB = new Point(4,3);
		$vertexC = new Point(3,5);
		$triangle = new Triangle($vertexA,$vertexB,$vertexC);

		$point = new Point(2,3);
		
		$this->assertTrue($triangle->isInside($point));
	}

	public function testNotContainsPoint(){
		$vertexA = new Point(0,2);
		$vertexB = new Point(4,3);
		$vertexC = new Point(3,5);
		$triangle = new Triangle($vertexA,$vertexB,$vertexC);

		$point = new Point(1,4);
		
		$this->assertFalse($triangle->isInside($point));
	}

	public function testContainsOrigin(){
		$vertexA = new Point(-3,-2);
		$vertexB = new Point(1,4);
		$vertexC = new Point(3,-2);
		$triangle = new Triangle($vertexA,$vertexB,$vertexC);

		$point = new Point(0,0);
		$this->assertTrue($triangle->isInside($point));
	}

	public function testNotContainsOrigin(){
		$vertexA = new Point(-2,5);
		$vertexB = new Point(0,2);
		$vertexC = new Point(2,5);
		$triangle = new Triangle($vertexA,$vertexB,$vertexC);

		$point = new Point(0,0);
		$this->assertFalse($triangle->isInside($point));
	}

	public function testPerimeter(){
		$vertexA = new Point(-2,5);
		$vertexB = new Point(0,2);
		$vertexC = new Point(2,5);
		$triangle = new Triangle($vertexA,$vertexB,$vertexC);
		
		$this->assertEquals(round($triangle->perimeter()), 11);
	}
}
