<?php
require_once(dirname(__FILE__) . '/../../app/models/solar-system/Planet.php');
require_once(dirname(__FILE__) . '/../../app/models/solar-system/Orbit.php');

use PHPUnit\Framework\TestCase;
use Models\SolarSystem\Planet;
use Models\SolarSystem\Orbit;

class PlanetTest extends TestCase{

	public function testInitializePlanet(){
		$planet = new Planet("Mars",new Orbit(200,5));
		$this->assertEquals($planet->getName(), "Mars");
		$this->assertEquals($planet->getOrbit()->getDistanceFromSun(), 200);
		$this->assertEquals($planet->getOrbit()->getAngle(), 0);
	}

	public function testInitializePlanetNoName(){
		$this->expectException(Exception::class);
		$orbit = new Orbit(200,5);
		$planet = new Planet(null, $orbit);
	}

	public function testInitializePlanetEmptyName(){
		$this->expectException(Exception::class);
		$orbit = new Orbit(200,5);
		$planet = new Planet("", $orbit);
	}

	public function testElapseDays(){
		$planet = new Planet("Mars",new Orbit(200,5,20));
		$planet->elapseDays(5);
		$this->assertEquals($planet->getOrbit()->getAngle(), 45);
	}

}