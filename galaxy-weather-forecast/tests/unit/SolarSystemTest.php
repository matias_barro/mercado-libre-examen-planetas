<?php
require_once(dirname(__FILE__) . '/../../app/models/solar-system/SolarSystem.php');
require_once(dirname(__FILE__) . '/../../app/models/solar-system/Planet.php');
require_once(dirname(__FILE__) . '/../../app/models/solar-system/Orbit.php');

use PHPUnit\Framework\TestCase;
use Models\SolarSystem\SolarSystem;
use Models\SolarSystem\Planet;
use Models\SolarSystem\Orbit;

class SolarSystemTest extends TestCase{

	protected $solarSystem;

	protected function setUp(){
		$this->solarSystem = new SolarSystem();
	}

	public function testInitializeSolarSystem(){
		$solarSystem = new SolarSystem();
		$this->assertEquals(count($solarSystem->getPlanets()), 0);
	}

	public function testAddPlanet(){
		$planet = new Planet("Mars", new Orbit(5,10));
		$this->solarSystem->addPlanet($planet);

		$planets = $this->solarSystem->getPlanets();

		$this->assertTrue(count($planets) == 1);
		$this->assertTrue(array_key_exists("Mars", $planets));
		$this->assertEquals($planets["Mars"]->getOrbit()->getDistanceFromSun(), 5);
		$this->assertEquals($planets["Mars"]->getOrbit()->getAngle(),0);
	}

	public function testAddPlanetSameName(){
		$this->expectException(Exception::class);

		$planet = new Planet("Mars", new Orbit(5,10));
		$this->solarSystem->addPlanet($planet);

		$otherPlanet = new Planet("Mars", new Orbit(6,20));
		$this->solarSystem->addPlanet($otherPlanet);
	}

	public function testAddPlanetSameDistanceFromSun(){
		$this->expectException(Exception::class);

		$planet = new Planet("Mars", new Orbit(10,10));
		$this->solarSystem->addPlanet($planet);

		$otherPlanet = new Planet("Venus", new Orbit(10,20));
		$this->solarSystem->addPlanet($otherPlanet);
	}

	public function testGetPlanetByName(){
		$planet = new Planet("Mars", new Orbit(10,10));
		$this->solarSystem->addPlanet($planet);

		$otherPlanet = new Planet("Venus", new Orbit(15,20));
		$this->solarSystem->addPlanet($otherPlanet);

		$venusPlanet = $this->solarSystem->getPlanetByName("Venus");
		$this->assertEquals($venusPlanet->getName(), "Venus");
		$this->assertEquals($venusPlanet->getOrbit()->getDistanceFromSun(), 15);
	}

	public function testGetPlanetByNameNotExists(){
		$this->expectException(Exception::class);

		$planet = new Planet("Mars", new Orbit(10,10));
		$this->solarSystem->addPlanet($planet);

		$venusPlanet = $this->solarSystem->getPlanetByName("Venus");
	}	

	public function testDeletePlanet(){
		$planet = new Planet("Mars", new Orbit(10,10));
		$this->solarSystem->addPlanet($planet);

		$otherPlanet = new Planet("Venus", new Orbit(15,20));
		$this->solarSystem->addPlanet($otherPlanet);

		$this->solarSystem->removePlanet("Mars");
		$planets = $this->solarSystem->getPlanets();

		$this->assertTrue(count($planets) == 1);
		$this->assertTrue(!array_key_exists("Mars", $planets));
		$this->assertTrue(array_key_exists("Venus", $planets));
	}

	public function testDeletePlanetThatNotExists(){
		$this->expectException(Exception::class);

		$planet = new Planet("Mars", new Orbit(10,10));
		$this->solarSystem->addPlanet($planet);

		$venusPlanet = $this->solarSystem->removePlanet("Venus");
	}

	public function testElapseDays(){
		$planet = new Planet("Mars", new Orbit(10,10,20));
		$this->solarSystem->addPlanet($planet);

		$otherPlanet = new Planet("Venus", new Orbit(15,-20));
		$this->solarSystem->addPlanet($otherPlanet);

		$this->solarSystem->elapseDays(5);

		$this->assertEquals($this->solarSystem->getPlanetByName("Mars")->getOrbit()->getAngle(), 70);
		$this->assertEquals($this->solarSystem->getPlanetByName("Venus")->getOrbit()->getAngle(), -100);
	}
}