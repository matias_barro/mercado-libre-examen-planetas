<?php
require_once(dirname(__FILE__) . '/../../app/classes/math/PointAlignment.php');
require_once(dirname(__FILE__) . '/../../app/classes/math/Point.php');

use PHPUnit\Framework\TestCase;
use Classes\Math\Point;
use Classes\Math\PointAlignment;

class PointAlignmentTest extends TestCase{

	public function testAlignedXVertex(){
		 $pointA = new Point(1,5);
		 $pointB = new Point(1,8);
		 $pointC = new Point(1,20);
		 
		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testAlignedYVertex(){
		 $pointA = new Point(2,3);
		 $pointB = new Point(8,3);
		 $pointC = new Point(10,3);
		 
		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testAlignedNegativeXVertex(){
		 $pointA = new Point(-10,5);
		 $pointB = new Point(-10,8);
		 $pointC = new Point(-10,20);

		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testAlignedNegativeYVertex(){
		 $pointA = new Point(2,-3);
		 $pointB = new Point(8,-3);
		 $pointC = new Point(10,-3);
		 
		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testAlignedPositiveSlopeOrigin(){
		 $pointA = new Point(-1,-3);
		 $pointB = new Point(0,0);
		 $pointC = new Point(1,3);
		 
		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testAlignedNegativeSlopeOrigin(){
		 $pointA = new Point(-6,10);
		 $pointB = new Point(0,0);
		 $pointC = new Point(3,-5);
		 
		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testAlignedPositiveSlope(){
		 $pointA = new Point(2,5);
		 $pointB = new Point(5,8);
		 $pointC = new Point(7,10);
		 
		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testAlignedNegativeSlope(){
		 $pointA = new Point(-3,3);
		 $pointB = new Point(-1,1);
		 $pointC = new Point(1,-1);
		 
		 $this->assertTrue(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testTwoAlignedXVertex(){
		 $pointA = new Point(1,5);
		 $pointB = new Point(3,6);
		 $pointC = new Point(1,7);
		 
		 $this->assertFalse(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testNotAlignedYVertex(){
		 $pointA = new Point(5,1);
		 $pointB = new Point(6,3);
		 $pointC = new Point(7,1);
		 
		 $this->assertFalse(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}
	
	public function testNotAlignedPositiveSlopeOrigin(){
		 $pointA = new Point(-2,-4);
		 $pointB = new Point(-1,3);
		 $pointC = new Point(2,6);
		 
		 $this->assertFalse(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}

	public function testNotAlignedNegativeSlopeOrigin(){
		 $pointA = new Point(-6,10);
		 $pointB = new Point(-2,8);
		 $pointC = new Point(1,3);
		 
		 $this->assertFalse(PointAlignment::areAligned($pointA,$pointB,$pointC));
	}
}