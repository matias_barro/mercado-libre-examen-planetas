<?php
require_once(dirname(__FILE__) . '/../../app/models/solar-system/FerengiSolarSystem.php');
use PHPUnit\Framework\TestCase;
use Models\SolarSystem\FerengiSolarSystem;
use Models\SolarSystem\Orbit;
use Models\SolarSystem\Planet;

class FerengiSolarSystemTest extends TestCase{

	protected $ferengiSolarSystem;

	protected function setUp(){
		$this->ferengiSolarSystem = new FerengiSolarSystem();
	}

	public function testFarengiPlanet(){
		$ferengiPlanet = $this->ferengiSolarSystem->getFerengiPlanet();
		$this->assertEquals($ferengiPlanet->getName(), FerengiSolarSystem::FERENGI_PLANET_NAME);
		$this->assertEquals($ferengiPlanet->getOrbit()->getDistanceFromSun(), 500);
	}

	public function testVulcanoPlanet(){
		$vulcanoPlanet = $this->ferengiSolarSystem->getVulcanoPlanet();
		$this->assertEquals($vulcanoPlanet->getName(), FerengiSolarSystem::VULCANO_PLANET_NAME);
		$this->assertEquals($vulcanoPlanet->getOrbit()->getDistanceFromSun(), 1000);
	} 

	public function testBetazoidePlanet(){
		$betazoidePlanet = $this->ferengiSolarSystem->getBetazoidePlanet();
		$this->assertEquals($betazoidePlanet->getName(), FerengiSolarSystem::BETAZOIDE_PLANET_NAME);
		$this->assertEquals($betazoidePlanet->getOrbit()->getDistanceFromSun(), 2000);
	} 

	public function testElapseDays(){
		$this->ferengiSolarSystem->elapseDays(5);

		$this->assertEquals($this->ferengiSolarSystem->getFerengiPlanet()->getOrbit()->getAngle(), 85);

		$this->assertEquals($this->ferengiSolarSystem->getVulcanoPlanet()->getOrbit()->getAngle(), 115);

		$this->assertEquals($this->ferengiSolarSystem->getBetazoidePlanet()->getOrbit()->getAngle(), 75);
	}
}