<?php
require_once(dirname(__FILE__) . '/../../app/models/solar-system/Orbit.php');
use PHPUnit\Framework\TestCase;
use Models\SolarSystem\Orbit;

class OrbitTest extends TestCase{

	protected $orbit;

	protected function setUp(){
		$this->orbit = new Orbit(10, 5, 10);
	}

	public function testInitializeOrbit(){
		$orbit = new Orbit(10,5,50);
		$this->assertEquals($orbit->getDistanceFromSun(), 10);
		$this->assertEquals($orbit->getAngle(), 50);
	}

	public function testInitializeOrbitZeroDistanceFromSun(){
		$this->expectException(Exception::class);
		$orbit = new Orbit(0,5);
	}

	public function testInitializeOrbitNegativeDistanceFromSun(){
		$this->expectException(Exception::class);
		$orbit = new Orbit(-2,5);
	}

	public function testInitializeOrbitNoInitialAngle(){
		$orbit = new Orbit(10,5);
		$this->assertEquals($orbit->getDistanceFromSun(), 10);
		$this->assertEquals($orbit->getAngle(), 0);
	}

	public function testElapseNegativeDays(){
		$this->expectException(Exception::class);
		$this->orbit->elapseDays(-2);
	}

	public function testElapseZeroDays(){
		$this->orbit->elapseDays(0);
		$this->assertEquals($this->orbit->getAngle(), 10);
	}

	public function testElapseOneDay(){
		$this->orbit->elapseDays(1);
		$this->assertEquals($this->orbit->getAngle(), 15);
	}

	public function testElapseSeveralDays(){
		$this->orbit->elapseDays(5);
		$this->assertEquals($this->orbit->getAngle(), 35);
	}

	public function testElapseSeveralDaysNegativeSpeed(){
		$orbit = new Orbit(2,-5,20);
		$orbit->elapseDays(6);
		$this->assertEquals($orbit->getAngle(), -10);
	}

	public function testCoordinates(){
		$this->assertEquals(round($this->orbit->getXCoordinate()), 10);
		$this->assertEquals(round($this->orbit->getYCoordinate()), 2);
	}

	public function testCoordinatesNegativeAngle(){
		$orbit = new Orbit(10,5,-30);
		$this->assertEquals(round($orbit->getXCoordinate()), 9);
		$this->assertEquals(round($orbit->getYCoordinate()), -5);
	}
}

