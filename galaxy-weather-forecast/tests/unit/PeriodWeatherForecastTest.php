<?php
require_once(dirname(__FILE__) . '/../../app/models/weather-forecast/PeriodWeatherForecast.php');
require_once(dirname(__FILE__) . '/../../app/models/weather-forecast/DailyWeatherForecast.php');
require_once(dirname(__FILE__) . '/../../app/models/weather-forecast/WeatherForecast.php');
require_once(dirname(__FILE__) . '/../../app/constants/WeatherTypes.php');


use PHPUnit\Framework\TestCase;
use Models\WeatherForecast\PeriodWeatherForecast;
use Models\WeatherForecast\DailyWeatherForecast;
use Models\WeatherForecast\WeatherForecast;
use Constants\WeatherTypes;

class PeriodWeatherForecastTest extends TestCase{

	public function testAddDailyForecast(){
		$periodForecast = new PeriodWeatherForecast();
		$periodForecast->addDailyForecast(new DailyWeatherForecast(1,new WeatherForecast(WeatherTypes::RAIN)));

		$periodForecast->addDailyForecast(new DailyWeatherForecast(2,new WeatherForecast(WeatherTypes::NORMAL)));

		$dailyForecast = $periodForecast->getDailyForecasts();
		$this->assertEquals(count($dailyForecast), 2);
		$this->assertEquals($dailyForecast[1]->getDay(),1);
		$this->assertEquals($dailyForecast[1]->getWeatherForecast()->getType(),WeatherTypes::RAIN);
		$this->assertEquals($dailyForecast[2]->getDay(),2);
		$this->assertEquals($dailyForecast[2]->getWeatherForecast()->getType(),WeatherTypes::NORMAL);
	}

	public function testAddDailyForecastDayAlreadyAdded(){
		$this->expectException(Exception::class);
		$periodForecast = new PeriodWeatherForecast();
		$periodForecast->addDailyForecast(new DailyWeatherForecast(1,new WeatherForecast(WeatherTypes::RAIN)));

		$periodForecast->addDailyForecast(new DailyWeatherForecast(1,new WeatherForecast(WeatherTypes::NORMAL)));
	}

	public function testCountDailyForecastByWeather(){
		$periodForecast = new PeriodWeatherForecast();
		$periodForecast->addDailyForecast(new DailyWeatherForecast(1,new WeatherForecast(WeatherTypes::RAIN)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(2,new WeatherForecast(WeatherTypes::RAIN)));

		$periodForecast->addDailyForecast(new DailyWeatherForecast(3,new WeatherForecast(WeatherTypes::DROUGHT)));

		$periodForecast->addDailyForecast(new DailyWeatherForecast(4,new WeatherForecast(WeatherTypes::OPTIMAL)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(5,new WeatherForecast(WeatherTypes::OPTIMAL)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(6,new WeatherForecast(WeatherTypes::OPTIMAL)));

		$this->assertEquals($periodForecast->getCountDailyForecastsByWeather(WeatherTypes::RAIN),2);
		$this->assertEquals($periodForecast->getCountDailyForecastsByWeather(WeatherTypes::DROUGHT),1);
		$this->assertEquals($periodForecast->getCountDailyForecastsByWeather(WeatherTypes::OPTIMAL),3);
	}

	public function testCountDailyForecastInvalidWeather(){
		$this->expectException(Exception::class);
		$periodForecast = new PeriodWeatherForecast();
		$periodForecast->addDailyForecast(new DailyWeatherForecast(1,new WeatherForecast(WeatherTypes::RAIN)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(2,new WeatherForecast(WeatherTypes::RAIN)));

		$invalidWeatherCount = $periodForecast->getCountDailyForecastsByWeather("sunny");
	}

	public function testGetPeriodMaxRainDay(){
		$periodForecast = new PeriodWeatherForecast();
		$periodForecast->addDailyForecast(new DailyWeatherForecast(10,new WeatherForecast(WeatherTypes::RAIN,30.6)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(8,new WeatherForecast(WeatherTypes::RAIN,80)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(120,new WeatherForecast(WeatherTypes::RAIN,12)));

		$this->assertEquals($periodForecast->getPeriodMaxRainDay()->getDay(),8);
	}


	public function testGetPeriodMaxRainDayNoRainyDays(){
		$this->expectException(Exception::class);
		$periodForecast = new PeriodWeatherForecast();
		$periodForecast->addDailyForecast(new DailyWeatherForecast(10,new WeatherForecast(WeatherTypes::DROUGHT,30.6)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(8,new WeatherForecast(WeatherTypes::DROUGHT,80)));
		$periodForecast->addDailyForecast(new DailyWeatherForecast(120,new WeatherForecast(WeatherTypes::OPTIMAL,12)));

		$this->assertEquals($periodForecast->getPeriodMaxRainDay()->getDay(),8);
	}
}