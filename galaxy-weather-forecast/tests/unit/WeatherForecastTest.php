<?php
require_once(dirname(__FILE__) . '/../../app/models/weather-forecast/WeatherForecast.php');
require_once(dirname(__FILE__) . '/../../app/constants/WeatherTypes.php');

use PHPUnit\Framework\TestCase;
use Models\WeatherForecast\WeatherForecast;
use Constants\WeatherTypes;

class WeatherForecastTest extends TestCase{

	public function testInitializeWeatherForecast(){
		$weatherForecast = new WeatherForecast(WeatherTypes::DROUGHT, 10);
		$this->assertEquals($weatherForecast->getType(), WeatherTypes::DROUGHT);
		$this->assertEquals($weatherForecast->getMagnitude(), 10);
	}

	public function testInitializeWeatherTypeNotExists(){
		$this->expectException(Exception::class);
		$weatherForecast = new WeatherForecast("cloudy", 10);
	}
}

