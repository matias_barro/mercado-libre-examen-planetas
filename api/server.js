var express = require('express');        // call express
var config  = require('config');
var mongoose   = require('mongoose');

var DailyWeatherForecast = require('./models/dailyWeatherForecast');

//initialize mongoose
mongoose.connect('mongodb://' + config.get('MongoDB.host') + ':' + config.get('MongoDB.port') + '/' + config.get('MongoDB.database'));

var app = express();// define our app using express

var port = process.env.PORT || 8080;

//TEST ROUTE
app.get('/', function(req, res) {
    res.send('Farengi Solar System - Weather Forecasts API');   
});

//CLIMA ROUTE
app.get('/clima', function(req,res){
	//get request day
	var day = req.param("dia",null);
	if(!day){
		return res.status(400).json({msg: "Debes ingresar un día"});
	}

	if(day <= 0){
		return res.status(400).json({msg: "El día ingresado deber ser mayor a cero"});
	}

	//search for forecast
	DailyWeatherForecast.findOne({"day": day}).exec(function(err, dailyForecast){
		if(err){
			return res.status(500).send(err);
		}

		if(dailyForecast){
			return res.json({dia: dailyForecast.day, clima: dailyForecast.weather});
		}else{
			return res.json({msg: "No se encontro pronóstico para el día ingresado."});
		}
	});
});

app.listen(port, function () {
  console.log('Weather Forecasts API listen on port ' + port);
});
