var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DailyWeatherForecastSchema = new Schema({
    day: Number,
    weather: String,
    magnitude: Number
},{collection: 'dailyweatherforecast'});

module.exports = mongoose.model('DailyWeatherForecast', DailyWeatherForecastSchema);