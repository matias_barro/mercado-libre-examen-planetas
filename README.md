# Mercado Libre - Examen Planetas

**Autor**: Matias Barro Beotegui  
**Email**: matias.barro@gmail.com  

## Tecnologías utilizadas ##
* **PHP** (7.0.16)
* **PHPUnit** (6.0.4)
* **MongoDB** (3.2.11)
* **NodeJS** (5.2.0)
  
## Script para pronósticos de clima ##
Dado un periodo de tiempo expresado en años, días y meses se puede obtener un resumen en donde se indica para cada clima la cantidad de días en los que se presentó. El archivo ***app.php*** ubicado en ***galaxy-weather-forecast/app/*** contiene el script a ejecutar y debe realizarse de la siguiente manera:  
  
***php galaxy-weather-forecast/app/app.php***  
  
Se debe proveer al menos uno de los siguientes parámetros:  
    
*y:* años del periodo de tiempo  
*m:* meses del periodo de tiempo  
*d:* días del periodo de tiempo  
  
**Ejemplo:**  
  
*php galaxy-weather-forecast/app/app.php -y10*  
![Ejemplo APP](images/ejemplo_ejecutar_app.png)
## Job para creación de modelo de datos ##
En el directorio ***galaxy-weather-forecast/app/jobs*** se encuentra el job 
***FerengiWeatherForecastDatabaseJob.php*** que realiza la carga de los pronósticos climáticos de un periodo de 10 años en una Base de datos de MongoDB, para luego poder realizar consultas utilizando una REST API.  
  
Se utilizó la librería [MongoDB PHP](https://github.com/mongodb/mongo-php-library) para realizar las inserciones de los documentos, la cual puede instalarse usando [Composer](https://getcomposer.org/) ejecutando el siguiente comando en ***galaxy-weather-forecast/app/*** :  
  
*** php composer.phar install *** (previo se debe realizar la instalación de composer)  
  
El job debe ejecutarse de la siguiente forma:  
  
*** php galaxy-weather-forecast/app/jobs/FerengiWeatherForecastDatabaseJob.php ***

## REST API ##
A través de una REST API se puede consultar el pronóstico de un día determinado en un periodo de 10 años.  
  
Esta api fue desarrollada utilizando **NodeJS** y las siguientes librerías:  
  
- [Config](https://www.npmjs.com/package/config) (^1.25.1)  
- [ExpressJS](http://expressjs.com/es/) (~4.0.0)   
- [Mongoose](http://mongoosejs.com/) (^4.8.4)  
  
La REST API fue hosteada en [Heroku](https://dashboard.heroku.com/login) utilizando [mLab](https://mlab.com/) como Database-as-a-service. La url para realizar las consultas  es la siguiente:  
  
[GET] [https://ferengiweatherforecast.herokuapp.com/clima](https://ferengiweatherforecast.herokuapp.com/clima)  
  
**Parametros:**  
  
- día: número de día que se quiere consultar (> 0)  
  
**Respuesta: (JSON)**  
  
- día: número de día consultado  
- clima: condición climática  
  
**Ejemplo:**  
![Ejemplo REST API](images/ejemplo_REST_API.png)